package ru.grigorev.animals;

/**
 * Created by Павел on 16.10.2014.
 */
public class Main {

    public static void main(String...args){

        Dog dog=new Dog();
        print(dog);
        Cat cat=new Cat();
        print(cat);
    }
    private static void print(Animals animals)
    {
        String str=
                animals.getName() +
                        " говорит " +
                        animals.getVoice();
        System.out.println(str);
    }
}
