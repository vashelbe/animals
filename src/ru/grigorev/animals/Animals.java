package ru.grigorev.animals;

/**
 * Created by Павел on 16.10.2014.
 */
public interface Animals {
    public String getName();
    public String getVoice();
}
