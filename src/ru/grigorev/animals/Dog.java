package ru.grigorev.animals;

import ru.grigorev.animals.Animals;

/**
 * Created by Администратор on 13.10.2014.
 */
public class Dog implements Animals {
    @Override
    public String getName() {
        return "Собака";
    }

    @Override
    public String getVoice() {
        return "гав-гав";
    }
}
