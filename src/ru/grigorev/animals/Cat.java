package ru.grigorev.animals;

import ru.grigorev.animals.Animals;

/**
 * Created by Администратор on 13.10.2014.
 */
public class Cat implements Animals {
    @Override
    public String getName() {
        return "Кот";
    }

    @Override
    public String getVoice() {
        return "Мяу-Мяу ёу";
    }
}